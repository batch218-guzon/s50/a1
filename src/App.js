import { useState, useEffect } from 'react';

import {UserProvider} from './UserContext';

import {Container} from 'react-bootstrap';

import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';

import AppNavbar from './components/AppNavbar';
import CourseView from './components/CourseView';

import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register'
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';

import './App.css';

// <fragment> = <>

function App() {

  // State hook for the user state
  // global scope
  // This will be used to store the user information and will be used for validating if a user is logged in on app or not
  // const [user, setUser] = useState({email: localStorage.getItem('email')});
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  // Function for clearing localStorage on logout
  const unsetUser = () => {
    localStorage.clear();
  }

  // used to check if the user information is properly stored upon login and the localStorage information is cleared upon logout
  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
        headers: {
            Authorization: `Bearer ${localStorage.getItem('token')}`
        }
    })
    .then(res => res.json())
    .then(data => {
        // user is logged in
        if(typeof data._id !== "undefined") {

            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        } 
        // user is logged out
        else { 
            setUser({
                id: null,
                isAdmin: null
            })
        }

    })
}, []);


  return (
    <>
    <UserProvider value={{user, setUser, unsetUser}}>
    {/*Initializes the dynamic routing*/}
    <Router>
      <AppNavbar />
      <Container>
        <Routes>
          <Route path="/" element={<Home />}  />
          <Route path="/courses" element={<Courses />} />
          <Route path="/courses/:courseId" element={<CourseView />} />
          <Route path="/register" element={<Register />} />                    
          <Route path="/login" element={<Login />} />                    
          <Route path="/logout" element={<Logout />} />
          {/*
              "*" - wildcard character that will match any path that was undefined in routes
          */}
          <Route path="/*" element={<Error />} />                    
        </Routes>
      </Container>
    </Router>
    </UserProvider>
    </>
  );
}

export default App;
