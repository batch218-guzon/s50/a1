import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';

// Activity S54
import {Navigate} from 'react-router-dom';

import UserContext from '../UserContext';
// Activity S54 end

export default function Register() {

    // Activity S54
    const {user} = useContext(UserContext);
    // Activity S54 end
    // State hooks to store the values of the input fields
    const [email, setEmail] = useState("");
    const [password1, setPassword1] = useState("");
    const [password2, setPassword2] = useState("");
    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false);

    console.log(email);
    console.log(password1);
    console.log(password2);

    // Function to simulate redirection via form submission
    function registerUser(e) {
        // prevents page redirection via form submission
        e.preventDefault()

        // Clear input fields
        setEmail("");
        setPassword1("");
        setPassword2("");
      
        alert('Thank you for registering!')
    }

    useEffect(() => {

      /*
      MINI ACTIVITY
      Create a validation to enable the submit button when all fields are populated and both passwords match
      */
      if((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)) {
        setIsActive(true);
      } else {
        setIsActive(false);
      }

    }, [email, password1, password2])

    return (
        // Activity S54 
        (user.id !== null) ?
        <Navigate to='/courses' />
        :
        // Activity S54 end
        <Form onSubmit={(e) => registerUser(e)}>
            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                  type="email" 
                  placeholder="Enter email"
                  value={email}
                  onChange={e => setEmail(e.target.value)}
                  required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>
            
            <Form.Group controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                  type="password" 
                  placeholder="Password" 
                  value={password1}
                  onChange={e => setPassword1(e.target.value)}
                  required
                />
            </Form.Group>
            <br />
            <Form.Group controlId="password2">
                <Form.Label>Verify Password</Form.Label>
                <Form.Control 
                  type="password" 
                  placeholder="Verify Password" 
                  value={password2}
                  onChange={e => setPassword2(e.target.value)}
                  required
                />
            </Form.Group>
                <br />
                { isActive ? 
                <Button variant="primary" type="submit" id="submitBtn">
                  Submit
                </Button>
                :
                <Button variant="danger" type="submit" id="submitBtn" disabled>
                  Submit
                </Button>
                }


            
        </Form>
    )

}