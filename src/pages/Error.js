// Activity S53

// import { Row, Col, Alert } from 'react-bootstrap';
// import{ Link } from 'react-router-dom';

// export default function Error() {
// return (
//     <Row>
//     	<Col className="p-5">
//             <p>Zuitt Booking</p>
//             <h1>Page Not Found</h1>
// {/*            <Alert variant="light">
//               Go back to the <Alert.Link as={Link} to="/">homepage</Alert.Link>
//             </Alert>*/}
//             <p>Go back to the <a href ="/">homepage</a></p>
//         </Col>
//     </Row>
// 	)
// }
// Activity End

import Banner from '../components/Banner'

export default function Error() {

    const data = {
        title: "Error 404 - Page not found.",
        content: "The page you are looking for cannot be found",
        destination: "/",
        label: "Back to Home"
    }

    return (
        <Banner data={data} />
    )
}