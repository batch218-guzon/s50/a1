import {useState, useEffect} from 'react';
import { Card, Button } from 'react-bootstrap';
import PropTypes from 'prop-types';

import{ Link } from 'react-router-dom';

export default function CourseCard({course}) {

  const {name, description, price, _id} = course;

  // count - getter
  // setCount - setter
  // useState(0) - useState(initialGetterValue)
  // const [count, setCount] = useState(0);
  // // S51 ACTIVITY
  // const [seats, setSeats] = useState(5);
  // // S51 ACTIVITY END
  // // console.log(useState(0));
  // const [isOpen, setIsOpen] = useState(true);

  // function that keeps track of the enrollees for a course
  function enroll() {
  //   setCount(count + 1)
  //   console.log('Enrollees: ' + count);
  // // S51 ACTIVITY
  //   setSeats(seats - 1)
  //   console.log('Seats: ' + seats)

    // if(seats === 1) {
    //   alert('No more seats left')
    //   document.querySelector(`#btn-enroll-${id}`).setAttribute('disabled', true)
    // }

  // S51 ACTIVITY END
  }

  // useEffect allows us to execute function if the value of seats state changes
  // useEffect(() => {
  //   if(seats === 0) {
  //     setIsOpen(false);
  //     alert('No more seats left');
  //     document.querySelector(`#btn-enroll-${id}`).setAttribute('disabled', true);
  //   }
  //   // will run anytime one of the values in the array of dependencies changes
  // }, [seats])

  // Checks to see if the data was successfully passed
  // console.log(props);
  // Every component receives information in a form of an object
  // console.log(typeof props);

  return (
    <Card style={{ width: 'auto' }}>
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Subtitle>Description:</Card.Subtitle>
        <Card.Text>{description}</Card.Text>
        <Card.Subtitle>Price:</Card.Subtitle>
        <Card.Text>PhP {price}</Card.Text>
        <Button className="bg-primary" as={Link} to={`/courses/${_id}`} >Details</Button>
        {/*<Card.Text>Enrollees: {count}</Card.Text>
        <Card.Text>Seats: {seats}</Card.Text>
        <Button id={'btn-enroll-' + id} className="bg-primary" onClick={enroll}>Enroll</Button>*/}
      </Card.Body>
    </Card>
  );
}

// "proptypes" - are a good way of checking data type of information between components.
CourseCard.propTypes = {
  // "shape" method is used to check if prop object conforms to a specific "shape"
  course: PropTypes.shape({
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired
  })
}