import React from 'react';

// "React.createContext()" - allows us to pass information between components without using props drilling
const UserContext = React.createContext();

// "provider" - allows other components to consume/use the context object and supply the necessary information needed to the context object
export const UserProvider = UserContext.Provider;

export default UserContext;